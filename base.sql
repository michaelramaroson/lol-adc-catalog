create database lolsite;
use lolsite;

create table Users
(
    id int not null AUTO_INCREMENT primary key,
    nom varchar(50),
    mdp varchar(50)
);

insert into Users values (null,'rojo','rojo');

create table champion
(
    id int not null AUTO_INCREMENT primary key,
    nom varchar(20),
    description varchar(150),
    histoire text,
	img varchar(20)
);

insert into Champion values (null,'Ashe','A chaque nouvelle fleche tiree de son arc antique enchante par la magie glace, Ashe est une archere hors pair.','Enfant, Ashe avait un temperament reveur. Elle etait fascinee devant les imposantes forteresses abandonnees de ses ancetres et restait des heures pres du feu a ecouter les legendes des anciens heros de Freljord. Sa preferee etait celle de Avarosa, la reine de ce Freljord auparavant, sublime et unifie. Meme si sa mere la grondait a chaque fois  car elle evoquait ce desir absurde, Ashe jura que, un jour, elle unirait elle aussi les tribus dispersees de la toundra. Au fond de Ashe, elle savait que si son peuple venait a se reunir comme autrefois, il atteindrait de nouveau un niveau excellent.','asheprofil.jpg');
insert into Champion values (null,'Vayne','Vayne est la chasseuse nocturne de LOL,capable de tuer meme les enemies les plus solides','Shauna Vayne est une chasseuse de monstres mortelle et sans scrupule. Originaire de Demacia, elle voue sa vie a la destruction du demon qui a assassine sa famille. Une arbalete montee sur son poignet et un coeur ronge par le desir de vengeance, elle est uniquement heureuse que quand elle assassine les praticiens ou les creations des arts obscurs en les frappant dans ombre avec une volee de carreaux en argent.','vayneprofil.jpg');



create table Capacite
(
    id int not null AUTO_INCREMENT primary key,
    idChampion int,
    nom varchar(50),
    description text,
	FOREIGN KEY(idChampion) REFERENCES Champion(id)
);

insert into Capacite values (null,1,'Tir givrant','Passif: Les attaques de Ashe ralentissent ses cibles et infligent des degats supplementaires aux cibles affectees.');
insert into Capacite values (null,1,'Concentrtion du ranger','Ashe genere des effets Concentration quand elle attaque. Elle les consomme et augmente temporairement sa vitesse de frappe et transforme son attaque de base en une puissante volee de fleches pendant la duree du sort.');
insert into Capacite values (null,1,'Salve','Ashe tire 9 fleches dans une zone conique pour infliger plus de degats. Applique aussi Tir givrant.');
insert into Capacite values (null,1,'Rapace','Ashe envoie son faucon en reconnaissance ou elle veut sur la carte.');
insert into Capacite values (null,1,'Fleche de cristal enchantee','Ashe tire un trait de glace en ligne droite. Si la fleche touche un champion ennemi, elle le blesse et etourdit ce dernier, la duree de cet etourdissement augmentant avec la distance parcourue.');


insert into Capacite values (null,2,'Chasseur nocturne','Passif: Vayne traque sans relache les etres malfaisants, augmentant sa vitesse de deplacement de 30 vers les champions ennemis proches.');
insert into Capacite values (null,2,'Roulade','Vayne fait une roulade et se positionne pour mieux viser. Sa prochaine attaque inflige un coup critique.');
insert into Capacite values (null,2,'Carreaux en argent','Passif: La troisieme attaque ou competence consecutive contre une meme cible inflige un pourcentage des PV max de la cible sous forme de degats bruts supplementaires.');
insert into Capacite values (null,2,'Condamnation','Vayne tire un carreau gigantesque sur sa cible, lui infligeant des degats et la projetant en arriere. Si la cible percute le decor, elle est empalee, ce qui lui inflige des degats supplementaires et étourdit celle-ci.');
insert into Capacite values (null,2,'Combat ultime','Vayne se prepare a un combat epique ; ses degats de frappe augmentent, elle devient invisible pendant Roulade et le bonus de vitesse de Chasseur nocturne est triple.');


create table Typebuild
(
    id int not null AUTO_INCREMENT primary key,
    nom varchar(50),
    img varchar(50)
);

insert into Typebuild values (null,'Vitesse de frappe','vitesse.jpg');
insert into Typebuild values (null,'Vol de vie','volvie.jpg');
insert into Typebuild values (null,'Coup critique','critique.jpg');
insert into Typebuild values (null,'Anti tank','tank.jpg');

create table Objetbuild
(
    id int not null AUTO_INCREMENT primary key,
    idType int,
	obj1 varchar(50),
	obj2 varchar(50),
	obj3 varchar(50),
	obj4 varchar(50),
	obj5 varchar(50),
	obj6 varchar(50),
	description varchar(150),
	FOREIGN KEY(idType) REFERENCES Typebuild(id)
);

insert into Objetbuild values (null,1,'Botte du berzerk','Statik','Lame du roi dechu','guinsoo','Canon ultrarapide','Danseur Fantome','Avec ce build, vous aurez 0 degat mais vous aurez une vitesse de frappe inegalee.');
insert into Objetbuild values (null,1,'Botte du berzerk','Statik','Lame du roi dechu','guinsoo','Canon ultrarapide','Lame infinie','Avec ce build, vous aurez degat, vitesse, et vol de vie.');
insert into Objetbuild values (null,1,'Danseur fantome','Statik','Lame du roi dechu','guinsoo','Canon ultrarapide','Lame infinie','Avec ce build, vous aurez encore plus de degat, vitesse, et vol de vie mais vous serez plus lent en deplacement.');


insert into Objetbuild values (null,2,'Botte du berzerk','Soif de sang','Lame du roi dechu','Danse de la mort','Cimeterre mercuriale','Danseur Fantome','Avec ce build, vous aurez tellement de vol de vie mais tres peu de chance de faire un coup critique.');
insert into Objetbuild values (null,2,'Botte du berzerk','Soif de sang','Lame du roi dechu','Lame infinie','Cimeterre mercuriale','Danseur Fantome','Avec ce build, vous aurez du vol de vie des coups critiques.');
insert into Objetbuild values (null,2,'Danse de la mort','Soif de sang','Lame du roi dechu','Lame infinie','Cimeterre mercuriale','Danseur Fantome','Avec ce build, vous aurez plus de vol de vie des coups critiques mais vous serez lent en deplacement.');


insert into Objetbuild values (null,3,'Botte du berzerk','Statik','Lame infinie','Faux spectrale','Canon ultrarapide','Ouragan de runaan','Avec ce build, vous aurez 100% de chance de coup critique et vous attaquerez rapidement.');
insert into Objetbuild values (null,3,'Botte du berzerk','Statik','Lame infinie','Faux spectrale','Canon ultrarapide','Soif de sang','Avec ce build, vous aurez 100% de chance de coup critique et du vol de vie.');
insert into Objetbuild values (null,3,'Danseur Fantome','Statik','Lame infinie','Faux spectrale','Canon ultrarapide','Soif de sang','Avec ce build, vous aurez 100% de chance de coup critique et du vol de vie et vous serez excellent en duel mais vous serez lent en deplacement.');

insert into Objetbuild values (null,4,'Botte du berzerk','Lamentation de dominique','Lame du roi dechu','Rappel mortel','Couperet noir','Lame infinie','Avec ce build, vous decouperez les tanks sans probleme.');
insert into Objetbuild values (null,4,'Botte du berzerk','Lamentation de dominique','Danseur fantome','Soif de sang','Couperet noir','Lame infinie','Avec ce build, vous decouperez les tanks, serez bon en duel et aurez des coups critiques.');