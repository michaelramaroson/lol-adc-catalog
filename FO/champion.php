<?php 
				include('fonction.php');
                $accueil2=listerChampion();    
				$aa=0;
				$aa2=0;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Description" content="catalog ADC League of Legends (LOL ADC Catalog). Liste Champion ADC. Site officiel des AD Carry LOL. Nom du Champion ADC. Histoire du Champion ADC. Nom du Champion ADC. Competence du Champion ADC.Voici la liste des Champions ADC de LOL du site LOL ADC Catalog.">
  <meta name="keywords" content="ADC, Carry, League of Legends, LOL, Champion ADC, Catalog, Histoire, Histoire ADC, Competence, Competence Champion ADC, Nom du Champion">
  <title>Champion ADC - Liste Champion ADC - Catalogue ADC - LOL ADC Catalog</title>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <link href="css/flexslider.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="tm-gray-bg">
  	<!-- Header -->
  	<div class="tm-header">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-4 col-md-3 col-sm-2 tm-site-name-container">
  					<a href="#" class="tm-site-name">LOL ADC Catalog</a>	
  				</div>
	  			<div class="col-lg-8 col-md-9 col-sm-10">
	  				<div class="mobile-menu-icon">
		              <i class="fa fa-bars"></i>
		            </div>
	  				<nav class="tm-nav">
						<ul>
							<li><a href="LOL-ADC-Catalog-accueil-site.html" >Accueil</a></li>
							<li><a href="LOL-ADC-Catalog-liste-champion-ADC.html" class="active">Champion ADC</a></li>
							<li><a href="LOL-ADC-Catalog-liste-build-ADC.html" >Build</a></li>
						</ul>
					</nav>		
	  			</div>				
  			</div>
  		</div>	  	
  	</div>
	
	
	
	<!-- Banner -->
	<section class="tm-banner">
		<!-- Flexslider -->
		<div class="flexslider flexslider-banner">
		  <ul class="slides">
		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title"><span class="tm-yellow-text">LOL ADC Catalog: Liste Champion ADC.</span></h1>
					<h2><span class="tm-yellow-text">Voici la liste des Champions ADC de LOL du site LOL ADC Catalog.</h2>
				</div>
				<img src="img/adc2.jpg" alt="ADC complet" title="Liste ADC" />	
		    </li>
		  </ul>
		</div>	
	</section>

	
	
			
	<section class="container tm-home-section-1" id="more">
	<div class="row">

	<div class="flexslider flexslider-about effect2">
		<h2 class="slider-title">Recherche d'un champion ADC</h2>
	<form method="post" action="LOL-ADC-Catalog-liste-champion-ADC-recheche-champion-specifique.html" id="forf">
			
				Entrer le nom du champion ADC si vous le connaissez, sinon vous pouvez feuilleter le catalogue ci dessous qui contient tous les champions ADC du site :<input style="width: 250px;" type="text" id="dmin" placeholder="nom champion" name="nomadc" class="form-control top">
				
                <button style="width: 150px;" class="btn btn-lg btn-primary btn-block" type="submit">Rechercher</button>
  </form>

	</div>
	</div>
</section>	




	
	
	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		<div class="row">
			<!-- slider -->
			<div class="flexslider flexslider-about effect2">
			  <ul class="slides">
			   
		
				<?php 
					foreach($accueil2 as &$var) 
				{
				?>
			
			   <li>
			      <img src="img/<?php  echo ($var['img']); ?>" alt="Champion ADC <?php  echo ($var['nom']); ?>" title="Champion ADC <?php  echo ($var['nom']); ?>" />
			      <div class="flex-caption">
			      	<h2 class="slider-title"><strong>Nom du Champion ADC:</strong> <?php  echo ($var['nom']); ?></h2>
			      	<h3 class="slider-subtitle"><strong>Description du Champion ADC:</strong> <?php  echo ($var['description']); ?></h3>
			      	<h4 class="slider-description"><strong>Histoire du Champion ADC:</strong> <?php  echo ($var['histoire']); ?></h4>
							<h4 class="slider-description">Si vous voulez plus d'informations, vous pouvez vous rendre sur <a href="https://euw.leagueoflegends.com/fr/game-info/champions/<?php echo ($var['nom']); ?>"></a> </h4>

							
			      </div>

				<h2 class="slider-title" align="center"><strong>Competences du Champion ADC:</strong> </h2>
				<?php 
					$aa=$var['id'];
					$accueil3=listerCapacite($aa);
					foreach($accueil3 as &$var2) 
				{
				?>
				
				<div class="flex-caption">
			      	
			      	<h3 class="slider-subtitle"><strong>Nom de la competence du Champion ADC:</strong> <?php  echo ($var2['nom']); ?></h3>
			      	<h4 class="slider-description"><strong>Description de la competence du Champion ADC:</strong> <?php  echo ($var2['description']); ?></h4>
			      </div>
				  
				<?php
				}
				?>
			    </li>
				
				<?php
				}
				?>
			   
			  </ul>
			</div>
		</div>
	
		
	</section>	






	
	<footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2084 Your Company Name</p>
			</div>
		</div>		
	</footer>
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      		<!-- jQuery -->
  	<script type="text/javascript" src="js/bootstrap.min.js"></script>					<!-- bootstrap js -->
  	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>			<!-- flexslider js -->
  	<script type="text/javascript" src="js/templatemo-script.js"></script>      		<!-- Templatemo Script -->
	<script>
		$(function() {

			// https://css-tricks.com/snippets/jquery/smooth-scrolling/
		  	$('a[href*=#]:not([href=#])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = $(this.hash);
			      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
		  	});		  	
		});
		$(window).load(function(){
			// Flexsliders
		  	$('.flexslider.flexslider-banner').flexslider({
			    controlNav: false
		    });
		  	$('.flexslider').flexslider({
		    	animation: "slide",
		    	directionNav: false,
		    	slideshow: false
		  	});
		});
	</script>
 </body>
 </html>
