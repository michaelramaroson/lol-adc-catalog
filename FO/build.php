<?php 
				include('fonction.php');
                $accueil2=listerTypebuild();    
				$aa=0;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Description" content="catalog ADC League of Legends (LOL ADC Catalog). Liste Build Champion ADC. Site officiel des AD Carry LOL. Voici la liste des builds pour les Champions ADC de LOL du site LOL ADC Catalog. Vol de vie. Vitesse de frappe. Coup critique. Anti tank">
  <meta name="keywords" content="ADC, Carry, League of Legends, LOL, Build ADC, Catalog, Vol de vie, Coup critique, Anti tank, Vitesse de frappe, Objet, Objet du Build">
  <title>Build ADC - Liste Build ADC - Catalogue ADC - LOL ADC Catalog</title>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <link href="css/flexslider.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="tm-gray-bg">
  	<!-- Header -->
  	<div class="tm-header">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-4 col-md-3 col-sm-2 tm-site-name-container">
  					<a href="#" class="tm-site-name">LOL ADC Catalog</a>	
  				</div>
	  			<div class="col-lg-8 col-md-9 col-sm-10">
	  				<div class="mobile-menu-icon">
		              <i class="fa fa-bars"></i>
		            </div>
	  				<nav class="tm-nav">
						<ul>
							<li><a href="LOL-ADC-Catalog-accueil-site.html" >Accueil</a></li>
							<li><a href="LOL-ADC-Catalog-liste-champion-ADC.html">Champion ADC</a></li>
							<li><a href="LOL-ADC-Catalog-liste-build-ADC.html"  class="active">Build</a></li>
						</ul>
					</nav>		
	  			</div>				
  			</div>
  		</div>	  	
  	</div>
	
	
	
	<!-- Banner -->
	<section class="tm-banner">
		<!-- Flexslider -->
		<div class="flexslider flexslider-banner">
		  <ul class="slides">
		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title"><span class="tm-yellow-text">LOL ADC Catalog: Liste des builds pour ADC.</span></h1>
					<h2><span class="tm-yellow-text">Voici la liste des builds pour les Champions ADC de LOL du site LOL ADC Catalog.</h2>
				</div>
				<img src="img/legend.jpg" alt="ADC complet" title="Liste ADC" />	
		    </li>
		  </ul>
		</div>	
	</section>
	
	<?php 
					foreach($accueil2 as &$var) 
				{
	?>
	
	
	<section class="container tm-home-section-1" id="more">
		
	
		<div class="section-margin-top">
			<div class="row">				
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<h2 class="tm-section-title"> <?php  echo ($var['nom']); ?></h2>
						
					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-3 col-sm-3"></div>
				<div class="col-lg-5 col-md-6 col-sm-6">
				<img src="img/<?php  echo ($var['img']); ?>" alt="image" class="img-responsive" width="300" height="300">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3"></div>
			</div>
				<?php 
					$aa=$var['id'];
					$accueil3=listerObjetbuild($aa);
					foreach($accueil3 as &$var2) 
				{
				?>	
			<div class="row">
				<div class="col-lg-12">
				<h3 align="center"><strong>Description du build:</strong> <?php  echo ($var2['description']); ?></h3>
					<h4 class="home-description">Liste des objets du build: <strong><?php  echo ($var2['obj1']); ?>, <?php  echo ($var2['obj2']); ?>, <?php  echo ($var2['obj3']); ?>, <?php  echo ($var2['obj4']); ?>, <?php  echo ($var2['obj5']); ?>, <?php  echo ($var2['obj6']); ?> </strong></h4>					
				</div>
			</div>	
				<?php
				}
				?>		
		</div>
		
		
	</section>	
	
	<?php
				}
	?>		
	
	
		
	
	
	
	<footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2084 Your Company Name</p>
			</div>
		</div>		
	</footer>
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      		<!-- jQuery -->
  	<script type="text/javascript" src="js/bootstrap.min.js"></script>					<!-- bootstrap js -->
  	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>			<!-- flexslider js -->
  	<script type="text/javascript" src="js/templatemo-script.js"></script>      		<!-- Templatemo Script -->
	<script>
		$(function() {

			// https://css-tricks.com/snippets/jquery/smooth-scrolling/
		  	$('a[href*=#]:not([href=#])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = $(this.hash);
			      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
		  	});		  	
		});
		$(window).load(function(){
			// Flexsliders
		  	$('.flexslider.flexslider-banner').flexslider({
			    controlNav: false
		    });
		  	$('.flexslider').flexslider({
		    	animation: "slide",
		    	directionNav: false,
		    	slideshow: false
		  	});
		});
	</script>
 </body>
 </html>
