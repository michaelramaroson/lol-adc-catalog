<?php 
														include('fonction.php');
														verify();
														$accueil2=listerCapaciteA();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7">   <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8">          <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9">                 <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js">                        <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>LOL ADC Catalog - Back office</title>
        <meta name="description" content="Metis: Bootstrap Responsive Admin Theme">
        <meta name="viewport" content="width=device-width">
        <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/css/bootstrap-responsive.min.css">
        <link type="text/css" rel="stylesheet" href="assets/Font-awesome/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="assets/css/style.css">
        <link type="text/css" rel="stylesheet" href="assets/css/calendar.css">

        <link rel="stylesheet" href="assets/css/theme.css">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE 7]>
        <link type="text/css" rel="stylesheet" href="assets/Font-awesome/css/font-awesome-ie7.min.css"/>
        <![endif]-->

        <script src="assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!-- BEGIN WRAP -->
        
		<div>


            <!-- BEGIN TOP BAR -->
            <div id="top">
                <!-- .navbar -->
                <div class="navbar navbar-inverse navbar-static-top">
                    <div class="navbar-inner">
                        <div class="container-fluid">
                           
                            <a class="brand" href="index.php">LOL ADC Catalog Back office</a>
                            <!-- .topnav -->
                            <div class="btn-toolbar topnav">
                               
                                <div class="btn-group">
                                    <a class="btn btn-inverse" data-placement="bottom" data-original-title="Logout" rel="tooltip"
                                       href="deconnexion.php"><i class="icon-off"></i></a>
								</div>
                            
							</div>
                            <!-- /.topnav -->
                            <div class="nav-collapse collapse">
                                <!-- .nav -->
                                <ul class="nav">
                                    <li class="active"><a href="index.php">Champion ADC</a></li>
                                    <li class="active"><a href="capacite.php">Capacite ADC</a></li>
                                    <li><a href="buildBO.php">Build</a></li>
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.navbar -->
            </div>
            <!-- END TOP BAR -->


            <!-- BEGIN HEADER.head -->
            <header class="head">
                <div class="main-bar">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="span12">
                                <h3><i class="icon-home"></i> Capacite ADC</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            

            <!-- BEGIN MAIN CONTENT -->
            <div>
                <!-- .outer -->
                <div>
                    <div>
                        <!-- .inner -->
                        <div>
                        <h2>Ajouter un Capacite a un champion ADC au site LOL ADC Catalog</h2>
						
                        <form method="post" action="insererCapacite.php">
               
							<input style="width: 250px;" type="number" placeholder="id du champion" name="idm" class="form-control"><br>
							<input style="width: 250px;" type="text" placeholder="nom de la capacite" name="nom" class="form-control"><br>
							<input style="width: 1200px;" type="textarea" placeholder="description champion" name="description" class="form-control"><br>

							
							<button style="width: 100px;" class="btn btn-lg btn-primary btn-block" type="submit">Ajouter</button>
						</form>   
                            
                            
                <h2>Liste des Capacites des ADC du site LOL ADC Catalog</h2>         

		 <table class="table table-bordered responsive">
                                                <thead>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>idChampion</th>
                                                        <th>nom</th>
                                                        <th>description</th>
                                                        <th>suppression</th>
                                                        <th>modification</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
												
												<?php 
														foreach($accueil2 as &$var) 
														{
												?>
												
                                                    <tr>
                                                        <td><?php  echo ($var['id']); ?></td>
                                                        <td><?php  echo ($var['idChampion']); ?></td>
                                                        <td><?php  echo ($var['nom']); ?></td>
                                                        <td><?php  echo ($var['description']); ?></td>
														<td><a href="suppressionCapacite.php?idm=<?php  echo ($var['id']); ?>"> Supprimer</a></td>
														<td><a href="modificationCapacite.php?idm=<?php  echo ($var['id']); ?>"> Modifier</a></td>
                                                    </tr>
												<?php 
														}
												?>
                                                </tbody>
        </table>
                            
                        
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- END CONTENT -->


            <!-- #push do not remove -->
            <div id="push"></div>
            <!-- /#push -->
        </div>
		
        <!-- END WRAP -->

        
		
		
		
		
		<div class="clearfix"></div>

        
		
		
		<!-- BEGIN FOOTER -->
        <div id="footer">
            <p>2013 © Metis Admin</p>
        </div>
        <!-- END FOOTER -->

        

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>



        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script>window.jQuery.ui || document.write('<script src="assets/js/vendor/jquery-ui-1.10.0.custom.min.js"><\/script>')</script>


        <script src="assets/js/vendor/bootstrap.min.js"></script>

        <script src="assets/js/lib/jquery.tablesorter.min.js"></script>

        <script src="assets/js/lib/jquery.mousewheel.js"></script>
        <script src="assets/js/lib/jquery.sparkline.min.js"></script>

        <script src="assets/flot/jquery.flot.js"></script>
        <script src="assets/flot/jquery.flot.pie.js"></script>
        <script src="assets/flot/jquery.flot.selection.js"></script>
        <script src="assets/flot/jquery.flot.resize.js"></script>

        <script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
        
        <script src="assets/js/main.js"></script>
        

        <script type="text/javascript">
            $(function() {
                dashboard();
            });
        </script>

        <script type="text/javascript" src="assets/js/style-switcher.js"></script>

    </body>
</html>
