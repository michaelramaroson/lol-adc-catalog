<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="Description" content="catalogue des ADC  League of Legends (LOL ADC Catalog). Site officiel des AD Carry LOL. C'est un champion generalement a distance. Infliger des degats d'attaque physique. Decouvrir des astuces supplementaires pour devenir un bon ADC.">
  <meta name="keywords" content="ADC, Carry, League of Legends, LOL, bon ADC, Catalog, catalogue, Attack Damage Carry, ADC dans LOL, ADC League of Lgends, counter pick">
  <title>Accueil - Catalogue ADC - LOL ADC Catalog</title>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <link href="css/flexslider.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="tm-gray-bg">
  	<!-- Header -->
  	<div class="tm-header">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-4 col-md-3 col-sm-2 tm-site-name-container">
  					<a href="#" class="tm-site-name">LOL ADC Catalog</a>	
  				</div>
	  			<div class="col-lg-8 col-md-9 col-sm-10">
	  				<div class="mobile-menu-icon">
		              <i class="fa fa-bars"></i>
		            </div>
	  				<nav class="tm-nav">
						<ul>
							<li><a href="LOL-ADC-Catalog-accueil-site.html"  class="active">Accueil</a></li>
							<li><a href="LOL-ADC-Catalog-liste-champion-ADC.html">Champion ADC</a></li>
							<li><a href="LOL-ADC-Catalog-liste-build-ADC.html" >Build</a></li>
						</ul>
					</nav>		
	  			</div>				
  			</div>
  		</div>	  	
  	</div>
	
	
	
	
	
	<!-- Banner -->
	<section class="tm-banner">
		<!-- Flexslider -->
		<div class="flexslider flexslider-banner">
		  <ul class="slides">
		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title"><span class="tm-yellow-text">Accueil du site LOL ADC Catalog.</span></h1>
					<h2><span class="tm-yellow-text">Accueil du site catalogue des ADC de League of Legends :LOL ADC Catalog.</h2>
				</div>
				<img src="img/ashe1.jpg" alt="ADC Ashe" title="Adc Ashe" />	
		    </li>
		  </ul>
		</div>	
	</section>

	
	
	
	
	
	
	
	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		
	
		<div class="section-margin-top">
			<div class="row">				
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<h2 class="tm-section-title"> Bienvenue sur le site officiel des AD Carry de League of Legend: LOL ADC Catalog. </h2>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
				<h3 align="center">Pour l'Accueil du site LOL ADC Catalog, expliquons ce qu'est un ADC.</h3>
					<h4 class="home-description">Un <strong>ADC</strong> ou Attack Damage Carry, c'est un <strong>champion generalement a distance</strong> qui depend de son equipement et donc de son farm pour <strong>infliger des degats d'attaque physique monstrueux</strong> en fin de partie. soit en top soit en bot avec un support pour assurer ses arrieres quand il farm. Souvent faible en debut de partie et tres fort à la fin, il est generalement accompagne d’un personnage support pour compenser sa faiblesse initiale et s’assurer qu’il atteindra la fin de partie dans de bonnes conditions. Autrement dit, ce sont les "tireurs". C'est eux qu'il faut absolument defendre en Team Fight car c'est eux qui vont detruire l'equipe adverse grace a leurs degats. C'est le poste le plus important et c'est pourquoi c'est lui qu'on appelle Carry ( ou porteur ) de l'equipe. Si vous etes un bon AD Carry alors votre equipe aura plus de chance de gagner. L'Accueil d'un bon ADC dans une equipe est donc la chose la plus voulue par tous les joueurs. Merci au site <strong>LOL ADC Catalog</strong> de nous indiquer comment bien jouer <strong>ADC</strong>.</h4>					
				</div>
			</div>			
		</div>
	</section>		
	
	
	
	
	
	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		
	
		<div class="section-margin-top">
			<div class="row">				
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<h2 class="tm-section-title"> LOL ADC Catalog pour decouvrir chaque ADC de LOL.</h2>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
				<h3 align="center">LOL ADC Catalog vous presente les ADC du jeu League of Legends.</h3>
					<h4 class="home-description">La liste des <strong>ADC</strong> est tres vaste:<strong> Ashe, Lucian, Varus...</strong> Chacun a son histoire, ses capacites, ses forces et ses faiblesses. Nous avons rassemble pour vous sur<strong>LOL ADC Catalog</strong> les informations consernant <a href="liste-champion-ADC.html"> les ADC de LOL </a> pour que vous puissiez decouvrir l'ADC qui vous convient apres avoir appris son histoire et apres avoir lu ses capacites.</h4>					
				</div>
			</div>			
		</div>
	</section>		
	
	
	
	
	
	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		
	
		<div class="section-margin-top">
			<div class="row">				
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<h2 class="tm-section-title">LOL ADC Catalog pour decouvrir des astuces supplementaires pour devenir un bon ADC.</h2>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
				<h3 align="center">Des petites astuces en plus expliquees dans l'Accueil du site LOL ADC Catalog.</h3>
					<h4 class="home-description">Mais comme ce poste est l'un des plus important dans une partie, alors vos adversaires vont faire en sorte de tout faire pour que vous ne puissiez rien faire dans la partie en prenant par exemple des objets qui limiteront vos degats et des champions qui contreront votre champion. Mais ne vous en faites pas, nous avons analyse ces situations et nous avons etabli des solutions pour vous sortir de ces mauvaises passe. SI vous voulez les connaitre, lisez les rubriques consacrees a ces dernieres. Vous verrez dans <strong>le coin superieur droit de l'Accueil de LOL ADC Catalog</strong> le lien pour <a href="liste-build-ADC.html">decouvrir les builds</a> des plus grands joueurs AD Carry du monde. </h4>					
				</div>
			</div>			
		</div>
	</section>		
	
	
	
	
	
	
	
	<footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2018 MicmicComp</p>
			</div>
		</div>		
	</footer>
	
	
	
	
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      		<!-- jQuery -->
  	<script type="text/javascript" src="js/moment.js"></script>							<!-- moment.js -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>					<!-- bootstrap js -->
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>	<!-- bootstrap date time picker js, http://eonasdan.github.io/bootstrap-datetimepicker/ -->
	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<!--
	<script src="js/froogaloop.js"></script>
	<script src="js/jquery.fitvid.js"></script>
-->
   	<script type="text/javascript" src="js/templatemo-script.js"></script>      		<!-- Templatemo Script -->
	<script>
		// HTML document is loaded. DOM is ready.
		$(function() {

			$('#hotelCarTabs a').click(function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})

        	$('.date').datetimepicker({
            	format: 'MM/DD/YYYY'
            });
            $('.date-time').datetimepicker();

			// https://css-tricks.com/snippets/jquery/smooth-scrolling/
		  	$('a[href*=#]:not([href=#])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = $(this.hash);
			      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
		  	});
		});
		
		// Load Flexslider when everything is loaded.
		$(window).load(function() {	  		
			// Vimeo API nonsense

/*
			  var player = document.getElementById('player_1');
			  $f(player).addEvent('ready', ready);
			 
			  function addEvent(element, eventName, callback) {
			    if (element.addEventListener) {
			      element.addEventListener(eventName, callback, false)
			    } else {
			      element.attachEvent(eventName, callback, false);
			    }
			  }
			 
			  function ready(player_id) {
			    var froogaloop = $f(player_id);
			    froogaloop.addEvent('play', function(data) {
			      $('.flexslider').flexslider("pause");
			    });
			    froogaloop.addEvent('pause', function(data) {
			      $('.flexslider').flexslider("play");
			    });
			  }
*/

			 
			 
			  // Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
/*

			  $(".flexslider")
			    .fitVids()
			    .flexslider({
			      animation: "slide",
			      useCSS: false,
			      animationLoop: false,
			      smoothHeight: true,
			      controlNav: false,
			      before: function(slider){
			        $f(player).api('pause');
			      }
			  });
*/


			  

//	For images only
		    $('.flexslider').flexslider({
			    controlNav: false
		    });


	  	});
	</script>
 </body>
 </html>