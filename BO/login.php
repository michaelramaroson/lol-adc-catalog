<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LOL ADC Catalog - BO - Login</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/login.css">
        <link rel="stylesheet" href="assets/magic/magic.css">
    </head>
    <body>
        <div class="container">
            <div class="text-center">
                <h1>LOL ADC Catalog</h1>
            </div>
            <div class="tab-content">
                <div id="login" class="tab-pane active">
                    <form action="connexion.php" class="form-signin" method="post">
                        <p class="muted text-center">
                            Enter your username and password
                        </p>
                        <input type="text" placeholder="Username" name="user" class="input-block-level">
                        <input type="password" placeholder="Password" name="password" class="input-block-level">
                        <button class="btn btn-large btn-primary btn-block" type="submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div> <!-- /container -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
        <script type="text/javascript" src="assets/js/vendor/bootstrap.min.js"></script>

        <script>
            $('.inline li > a').click(function() {
                var activeForm = $(this).attr('href') + ' > form';
                //console.log(activeForm);
                $(activeForm).addClass('magictime swap');
                //set timer to 1 seconds, after that, unload the magic animation
                setTimeout(function() {
                    $(activeForm).removeClass('magictime swap');
                }, 1000);
            });

        </script>
    </body>
</html>
