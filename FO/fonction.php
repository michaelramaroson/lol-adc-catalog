<?php
	
	function getConnectionBase(){
		$user='root';
		$mdp='root';
		$dsn='mysql:host=localhost;port=3306;dbname=lolsite';
		$connexion=new PDO($dsn, $user, $mdp);
		return $connexion;
	}
	
	
	//Fonction de liste
	
	function listerChampion(){
		$connexion=getConnectionBase();
		$sql1="SELECT * from champion order by id asc";
		$sql1=sprintf($sql1);
		$res =$connexion->query($sql1);
		$i=0;
		$res->setFetchMode(PDO::FETCH_ASSOC);
		while($var=$res->fetch()) {
			$arr[$i]=$var;
			$i++;
		}
		return $arr;
	}

	function listerChampionNom($ps){
		$connexion=getConnectionBase();
		$sql1="SELECT * from champion where nom like '%".$ps."%'";
		
		$res =$connexion->query($sql1);
		$res->setFetchMode(PDO::FETCH_OBJ);
		return $res;
	}
	
	function listerCapacite($ps){
		$connexion=getConnectionBase();
		$sql1="SELECT * from Capacite where idChampion=%d";
		$sql1=sprintf($sql1,$ps);
		$res =$connexion->query($sql1);
		$i=0;
		$res->setFetchMode(PDO::FETCH_ASSOC);
		while($var=$res->fetch()) {
			$arr[$i]=$var;
			$i++;
		}
		return $arr;
	}
	
	function listerTypebuild(){
		$connexion=getConnectionBase();
		$sql1="SELECT * from Typebuild";
		$sql1=sprintf($sql1);
		$res =$connexion->query($sql1);
		$i=0;
		$res->setFetchMode(PDO::FETCH_ASSOC);
		while($var=$res->fetch()) {
			$arr[$i]=$var;
			$i++;
		}
		return $arr;
	}
	
		function listerObjetbuild($ps){
		$connexion=getConnectionBase();
		$sql1="SELECT * from Objetbuild where idType=%d";
		$sql1=sprintf($sql1,$ps);
		$res =$connexion->query($sql1);
		$i=0;
		$res->setFetchMode(PDO::FETCH_ASSOC);
		while($var=$res->fetch()) {
			$arr[$i]=$var;
			$i++;
		}
		return $arr;
	}

	


?>